#!/bin/bash

echo | openssl s_client -showcerts -connect $1:443 | openssl x509 -noout -dates
