#!/bin/bash

echo "IP";
echo "";
hostname=("$1");
host $hostname;
host $hostname  | grep "has address" | awk '{print $4}' > /tmp/hostlist.txt;
echo $hostname 
echo "";
echo "Reverse DNS";
echo "";
while read line; do
	host $line;
done < /tmp/hostlist.txt;
rm -rf /tmp/hostlist.txt;
echo"";
